﻿using ColoDAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ColoDAL.Services
{
    public class JsFrameworkService : IJsFrameworksService
    {
        List<JsFramework> jsFrameworks = new List<JsFramework>() { new JsFramework() {Name ="Dupa" } };

        public void AddFramework(JsFramework framework) => jsFrameworks.Add(framework);

        public List<JsFramework> GetFrameworks() => jsFrameworks;

        
    }
}
