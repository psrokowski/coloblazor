﻿using ColoDAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ColoDAL.Services
{
    public interface IJsFrameworksService
    {
        List<JsFramework> GetFrameworks();

        void AddFramework(JsFramework framework);
    }
}
